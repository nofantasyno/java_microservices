package com.example.reportsservice.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.*;

@Service
public class ReportService {
    @Autowired
    RestTemplate restTemplate;


    @HystrixCommand(fallbackMethod = "callOrderService_Fallback")
    public HashMap<String, String> callOrderService() throws IOException {
        Order[] response = restTemplate
                .getForEntity("http://localhost:8090/api/orders", Order[].class).getBody();

        HashMap<String, String> map = new HashMap<String, String >();
        int orders_num = response.length;
        int orders_sum = 0;
        int products_num = 0;
        for(int i=0;i<orders_num; i++){
            orders_sum +=response[i].getTotalPrice();
            products_num +=response[i].getTotalQuantity();
        }
        map.put("orders_num", String.valueOf(orders_num));
        map.put("orders_sum", String.valueOf(orders_sum));
        map.put("products_num", String.valueOf(products_num));
        return map;
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @SuppressWarnings("unused")
    private HashMap<String, String> callOrderService_Fallback() {
        HashMap<String, String> response = new HashMap<>();
        response.put("orders_num", String.valueOf(0));
        response.put("orders_sum", String.valueOf(0));
        response.put("products_num", String.valueOf(0));
        return response;
    }
}

