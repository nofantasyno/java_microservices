package com.example.ordersmicroservice;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

@Service
public class OrderService {
    @Autowired
    RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "callProductsService_Fallback")
    public ArrayList callProductsService() {
        ArrayList response = restTemplate
                .getForEntity("http://localhost:8091/api/products", ArrayList.class).getBody();

        return response;
    }

    @SuppressWarnings("unused")
    private ArrayList callProductsService_Fallback() {
        ArrayList response = new ArrayList();
        response.add("Sorry, products_service is unavailable now :(");
        return response;
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
