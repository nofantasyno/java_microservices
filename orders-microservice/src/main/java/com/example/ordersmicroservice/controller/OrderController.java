package com.example.ordersmicroservice.controller;

import com.example.ordersmicroservice.OrderService;
import com.example.ordersmicroservice.exception.ResourceNotFoundException;
import com.example.ordersmicroservice.model.Order;
import com.example.ordersmicroservice.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@Controller
@RequestMapping("/api")
public class OrderController {
    private OrderRepository orderRepository;

    @Autowired
    public OrderController(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @GetMapping("/orders")
    @ResponseBody
    public List<Order> getAllItems(@RequestParam(value = "page", required = false, defaultValue = "0") int page, @RequestParam(value = "sort", required = false, defaultValue = "createdAt") String sort) {
        //Pageable pageable = PageRequest.of(page, 1000, Sort.by(sort));
        return orderRepository.findAll();
    }

    @CrossOrigin(origins = "http://localhost:8090")
    @PostMapping("/orders")
    @ResponseBody
    public Order parse( @RequestBody Order newOrder/*@RequestParam(value = "clientName", required = true) String clientName,
                       @RequestParam(value = "clientAddress", required = false) String clientAddress,
                       @RequestParam(value = "clientComment", required = true) String clientComment,
                       @RequestParam(value = "totalPrice", required = true) Integer totalPrice,
                       @RequestParam(value = "totalQuantity", required = true) Integer totalQuantity,
                       @RequestParam(value = "itemId", required = true) Long itemId*/){

        Order order = null;

        try {
            order = new Order(newOrder.getClientName(), newOrder.getClientAddress(), newOrder.getClientComment(), newOrder.getTotalPrice(), newOrder.getTotalQuantity(), newOrder.getItemId());
        }catch (Exception e){
            e.printStackTrace();
        }

        this.orderRepository.save(order);

        return order;
    }


    @GetMapping("/orders/{id}")
    @ResponseBody
    public Order getOrderById(@PathVariable(value = "id") Long orderId) {
        return orderRepository.findById(orderId)
                .orElseThrow(() -> new ResourceNotFoundException("Order", "id", orderId));
    }

    @PutMapping("/orders/edit/{id}")
    public Order updateOrder(@PathVariable(value = "id") Long orderId,
                           @RequestBody Order orderDetails) {

        Order order = orderRepository.findById(orderId)
                .orElseThrow(() -> new ResourceNotFoundException("Order", "id", orderId));

        order.setClientAddress(orderDetails.getClientAddress());
        order.setClientComment(orderDetails.getClientComment());
        order.setClientName(orderDetails.getClientName());
        order.setTotalPrice(orderDetails.getTotalPrice());
        order.setTotalQuantity(orderDetails.getTotalQuantity());
        order.setItemId(orderDetails.getItemId());

        return orderRepository.save(order);
    }

    @DeleteMapping("/orders/{id}")
    @ResponseBody
    public List<Order> deleteOrder(@PathVariable(value = "id") Long orderId) {
        Order order = orderRepository.findById(orderId)
                .orElseThrow(() -> new ResourceNotFoundException("Order", "id", orderId));

        orderRepository.delete(order);

        return orderRepository.findAll();
    }


    @Autowired
    OrderService orderService;

    @RequestMapping(value = "/orders/products", method = RequestMethod.GET)
    public ArrayList getProducts() {
        return orderService.callProductsService();
    }

}
