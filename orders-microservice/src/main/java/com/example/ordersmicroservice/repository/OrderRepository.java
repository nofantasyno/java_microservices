package com.example.ordersmicroservice.repository;

import com.example.ordersmicroservice.model.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    Page<Order> findAll(Pageable pageable);

    //Page<Item> findByPrice(Integer price, Pageable pageable);

    //Slice<Item> findByFirstNameAndLastName(String firstName, String lastName, Pageable pageable);

}