package com.example.reportsservice.controller;

import com.example.reportsservice.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashMap;

@RestController
@Controller
@RequestMapping("/api")
public class ReportsController {

    @Autowired
    ReportService reportService;

    @RequestMapping(value = "/reports/common_report", method = RequestMethod.GET)
    public HashMap<String, String> getOrders() throws IOException {
        //ObjectMapper mapper = new ObjectMapper();

        return reportService.callOrderService();
    }
}
