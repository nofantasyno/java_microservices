package com.eureka.zuul.service;

import com.eureka.zuul.model.User;
import com.eureka.zuul.web.dto.UserRegistrationDto;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    User findByEmail(String email);

    User save(UserRegistrationDto registration);
}
