package com.example.reportsservice.service;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Value;

import java.io.Serializable;
import java.util.Date;

@JsonSerialize
@JsonPropertyOrder({
        "id",
        "itemId",
        "clientName",
        "clientAddress",
        "clientComment",
        "totalPrice",
        "totalQuantity",
        "createdAt",
        "updatedAt"})
public class Order implements Serializable {

    @JsonProperty("id")
    public Long id;

    @JsonProperty("itemId")
    public Long itemId;

    @JsonProperty("clientName")
    public String clientName;

    @JsonProperty("clientAddress")
    public String clientAddress;

    @JsonProperty("clientComment")
    public String clientComment;

    @JsonProperty("totalPrice")
    public Integer totalPrice;

    @JsonProperty("totalQuantity")
    public Integer totalQuantity;

    @JsonProperty("createdAt")
    public Date createdAt;

    @JsonProperty("updatedAt")
    public Date updatedAt;

    public Order(Long id, Long itemId, String clientName, String clientAddress, String clientComment, Integer totalPrice, Integer totalQuantity) {
        this.id = id;
        this.itemId = itemId;
        this.clientName = clientName;
        this.clientAddress = clientAddress;
        this.clientComment = clientComment;
        this.totalPrice = totalPrice;
        this.totalQuantity = totalQuantity;
    }

    public Order() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    public String getClientComment() {
        return clientComment;
    }

    public void setClientComment(String clientComment) {
        this.clientComment = clientComment;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Integer getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Integer totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
